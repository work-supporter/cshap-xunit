using System;
using Xunit;

namespace ForCoyaSystemTests
{
    public class UnitTest1
    {
        [Fact]
        public void Pass()
        {
            var actual = ForCoyaSystem.Class1.Add(1, 3);
            Assert.Equal(4, actual);
        }
    }
}
